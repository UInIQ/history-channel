import * as Sequelize from "sequelize";

export class ChannelModel {
    protected sql: Sequelize.Sequelize;
    protected models: Sequelize.ModelsHashInterface;
    public model: Sequelize.Model<any, any>;

    constructor(sql: Sequelize.Sequelize) {
        this.sql = sql;
        this.models = this.sql.models;
        this.defineModel();
    }

    private defineModel(): void {
        this.sql.define("channel", {
            id: {
                type: Sequelize.STRING,
                primaryKey: true
            },
            name: Sequelize.STRING,
            is_channel: Sequelize.BOOLEAN,
            is_group: Sequelize.BOOLEAN,
            is_im: Sequelize.BOOLEAN,
            created: Sequelize.DATE,
            creator: {
                type: Sequelize.STRING,
                references: {
                    model: "users",
                    key: "id"
                }
            },
            is_archived: Sequelize.BOOLEAN,
            is_general: Sequelize.BOOLEAN,
            name_normalized: Sequelize.STRING,
            is_read_only: Sequelize.BOOLEAN,
            is_private: Sequelize.BOOLEAN,
            is_mpim: Sequelize.BOOLEAN,
            topic_value: Sequelize.STRING,
            topic_creator: {
                type: Sequelize.STRING,
                references: {
                    model: "users",
                    key: "id"
                }
            },
            topic_last_set: Sequelize.DATE,
            purpose_value: Sequelize.STRING,
            purpose_creator: {
                type: Sequelize.STRING,
                references: {
                    model: "users",
                    key: "id"
                }
            },
            purpose_last_set: Sequelize.DATE,
            locale: Sequelize.STRING,
        }, {
            indexes: [
                {
                    fields: ["id", "name"],
                    where: {
                        is_mpim: false
                    }
                }
            ]
        });

        this.model = this.models.channel;
    }

    public associateModels(): void {
        this.models.channel.belongsToMany(this.models.user, {through: "user_channels"});
        this.models.channel.hasMany(this.models.message, {foreignKey: "channel"});
    }
}