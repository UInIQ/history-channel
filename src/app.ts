import { WebClient } from "@slack/client";
import * as Sequelize from "sequelize";
import * as delay from "delay";

import * as Models from "./models/index";

const config = require("../config.json");

const sql = new Sequelize(config.sequelize);
const web = new WebClient(config.slack.token);

let registered = {};

let Users: Sequelize.Model<any, any>;
let Channels: Sequelize.Model<any, any>;
let Messages: Sequelize.Model<any, any>;

setUpDB();

async function setUpDB (): Promise<void> {
    for (let model of Object.keys(Models)) {
        registered[model] = new Models[model](sql);
    }
    
    for (let model in registered) {
        registered[model].associateModels();
    }
    await sql.sync();

    Users = registered["UserModel"].model;
    Channels = registered["ChannelModel"].model;
    Messages = registered["MessageModel"].model;
    
    populateDB();
}

async function populateDB (): Promise<void> {
    console.log("Fetching channel and user info from Slack...");

    let channelResp = await getChannels();
    let userResp = await getUsers();
    let channelMembership = {};

    for (let channel of channelResp) {
        channelMembership[channel.id] = await getChannelMembership(channel.id);
    }

    console.log("Writing information to database...");
    await Users.bulkCreate(userResp);
    await Channels.bulkCreate(channelResp);
    
    let usersDb = await Users.findAll();
    for (let channelId in channelMembership) {
        let channel = await Channels.findOne({ where: { id: channelId }});

        // This is so it doesn't complain about the user property not existing
        (channel as any).setUsers(channelMembership[channelId]);
    }

    console.log("Retrieving message history (warning: this might take some time)...");
    for (let channelId in channelMembership) {
        let channelName = await Channels.findOne({where: {id: channelId}}).then(channel => channel.name);
        console.log(`\tProcessing #${channelName}...`);
        
        let messages = await getMessages(channelId);
        await Messages.bulkCreate(messages); 
        
        console.log(`\t\tFinished processing ${messages.length} messages in #${channelName}.`);
    }
}

async function getUsers (cursor: string | null = null, users: any[] = []): Promise<any> {
    let results = await web.users.list(cursor === null ? undefined : {cursor: cursor});

    for (let user of results.members) {
        let entry = {
            id: user.id as string,
            name: user.name as string,
            deleted: user.deleted as boolean,
            tz: user.tz as string,
            is_admin: user.is_admin || false as boolean,
            is_bot: user.is_bot || false as boolean,
            is_app_user: user.is_app_user || false as boolean,
            team_id: user.team_id || null as string,
            color: user.color || null as string,
            tz_label: user.tz_label || null as string,
            tz_offset: user.tz_offset || 0 as number,
            profile_avatar_hash: user.profile.avatar_hash || null as string,
            profile_status_text: user.profile.status_text || null as string,
            profile_status_emoji: user.profile.status_emoji || null as string,
            profile_real_name: user.profile.real_name || null as string,
            profile_display_name: user.profile.display_name || null as string,
            profile_real_name_normalized: user.profile.real_name_normalized || null as string,
            profile_display_name_normalized: user.profile.display_name_normalized || null as string,
            profile_email: user.profile.email || null as string,
            profile_image_24: user.profile.image_24 as string,
            profile_image_32: user.profile.image_32 as string,
            profile_image_48: user.profile.image_48 as string,
            profile_image_72: user.profile.image_72 as string,
            profile_image_192: user.profile.image_192 as string,
            profile_image_512: user.profile.image_512 as string,
            profile_team: user.profile.team || null as string,
            is_owner: user.is_owner || false as boolean,
            is_primary_owner: user.is_primary_owner || false as boolean,
            is_restricted: user.is_restricted || false as boolean,
            is_ultra_restricted: user.is_ultra_restricted || false as boolean,
            is_stranger: user.is_stranger || false as boolean,
            updated: new Date(Number(user.updated) * 1000),
            has_2fa: user.has_2fa || false as boolean,
            locale: user.locale || null as string
        }

        users.push(entry);
    }

    if (results.hasOwnProperty("response_metadata")  
            && results.response_metadata.hasOwnProperty("next_cursor")
            && results.response_metadata.next_cursor != "") {
        await delay(1200);
        return await getUsers(results.response_metadata.next_cursor, users);
    } else {
        return users;
    }
}

async function getChannels (cursor: string | null = null, channels: any[] = [], getPrivate: boolean = false): Promise<any> {
    let results;
    
    // Yeah, I know this is stupid, but hey, it works. 
    if (!getPrivate) {
        results = await web.channels.list(cursor === null ? undefined : { cursor: cursor });
    } else {
        results = await web.groups.list(cursor === null ? undefined : { cursor: cursor });
        results.channels = results.groups;
    }

    for (let channel of results.channels) {
        let entry = {
            id: channel.id as string,
            name: channel.name as string,
            is_channel: channel.is_channel || true,
            is_group: channel.is_group || false,
            is_im: channel.is_im || false,
            created: new Date(Number(channel.created) * 1000),
            creator: channel.creator,
            is_archived: channel.is_archived || false,
            is_general: channel.is_general || false,
            name_normalized: channel.name_normalized || "",
            is_read_only: channel.is_read_only || false,
            is_private: channel.is_private || false,
            is_mpim: channel.is_mpim || false,
            topic_value: channel.topic.value || null as string,
            topic_creator: channel.topic.creator || null as string,
            topic_last_set: new Date(Number(channel.topic.last_set) * 1000) || null as Date,
            purpose_value: channel.purpose.value || null as string,
            purpose_creator: channel.purpose.creator || null as string,
            purpose_last_set: new Date(Number(channel.purpose.last_set) * 1000) || null as Date,
            locale: channel.locale || "en-US"
        }

        if (!entry.is_mpim) channels.push(entry);
    }

    if (results.hasOwnProperty("response_metadata") 
            && results.response_metadata.hasOwnProperty("next_cursor")
            && results.response_metadata.next_cursor != "") {
        await delay(1200);
        return await getChannels(results.response_metadata.next_cursor, channels, getPrivate);
    } else {
        if (!getPrivate) {
            await delay(1200); // This is to avoid being rate limited
            return await getChannels(null, channels, !getPrivate);
        } else {
            return channels;
        }
    }
}

async function getMessages(channel: string, cursor: string | null = null, messages: any[] = []): Promise<any> {
    // The limit property can be set up to 1000, but they recommend keeping it at 200. 
    let results = await web.conversations.history(channel, {limit: 200, cursor: cursor === null ? undefined : cursor});

    for (let message of results.messages) {
      let entry = {
        time: new Date(Number(message.ts) * 1000),
        ts: message.ts,
        type: message.type,
        subtype: message.subtype || null as string,
        text: message.text,
        channel: channel,
        user: message.user
      };

      messages.push(entry);

      if (messages.length % 10000 == 0) console.log(`\t\tProcessed ${messages.length} messages so far`);
    }

    if (results.hasOwnProperty("response_metadata") 
            && results.response_metadata.hasOwnProperty("next_cursor")
            && results.response_metadata.next_cursor != "") {
        await delay(1200);
        return await getMessages(channel, results.response_metadata.next_cursor, messages);
    } else {
        return messages;
    }
}

async function getChannelMembership(channel: string, cursor: string | null = null, members: any[] = []): Promise<any> {
    let results = await web.conversations.members(channel, {limit: 200, cursor: cursor === null ? undefined : cursor});

    members = members.concat(results.members);

    if (results.hasOwnProperty("response_metadata") 
            && results.response_metadata.hasOwnProperty("next_cursor")
            && results.response_metadata.next_cursor != "") {
        await delay(1200);
        return await getChannelMembership(channel, results.response_metadata.next_cursor, members);
    } else {
        return members;
    }
}